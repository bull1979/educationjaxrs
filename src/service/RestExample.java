package service;

import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import business.Person;
import main.webapp.AppHandler;

@Path("/rest")
public class RestExample {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getJSON(){
		
		
		
		return "{\"test\":\"test\"}";
	}
	
	
	@POST
	@Path("sendPerson/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String postData(Person p){
		
		System.out.println(p.getVorname() + " " + p.getName());
		return "{\"status\":\"ok\"}";
	}
	
	@GET
	@Path("addToGame/")
	@Produces(MediaType.APPLICATION_JSON)
	public String addTogame(){
		
		String token = AppHandler.addToGame(); //deine klasse ruft token auf
		return "{\"Token\":\""+token+"\"}";
	}
	
}
